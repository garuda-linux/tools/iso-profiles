# Set a few environment variables for the live session

export XDG_SESSION_TYPE=wayland
export XDG_SESSION_DESKTOP=sway
export XDG_CURRENT_DESKTOP=sway
export QT_QPA_PLATFORM=wayland
export QT_QPA_PLATFORMTHEME=gtk3
export QT_STYLE_OVERRIDE=kvantum
export GTK_THEME=Nordic-darker
export EDITOR=/usr/bin/micro
export TERM=foot

